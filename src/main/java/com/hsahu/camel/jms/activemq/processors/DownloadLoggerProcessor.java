package com.hsahu.camel.jms.activemq.processors;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

/**
 * Just printing received ftp file names
 * 
 * @author hsahu
 *
 */
public class DownloadLoggerProcessor implements Processor {

	public void process(Exchange exchange) throws Exception {
		System.out.println("Just Downloaded: " + exchange.getIn().getHeader(Exchange.FILE_NAME));
	}
}
