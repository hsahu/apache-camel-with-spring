### FTP and ActiveMQ JMS Integration

This package will contains files for integration of ftp and activemq using spring and apache camel.

CamelContext and beans will be loaded from [ftp-activemq-camel-context.xml](../../../../../../resources/ftp-activemq-camel-context.xml) file.

In this application we will download files from a directory of a ftp and publish the message in JMS queue. Also we will consume the messages from activeMQ JMS queue and print the content of message.
