package com.hsahu.camel.jms.activemq.processors;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

/**
 * Just printing received jms messages
 * 
 * @author hsahu
 *
 */
public class JMSMessageLoggerProcessor implements Processor {

	public void process(Exchange exchange) throws Exception {
		System.out.println("Consumed Message from JMS: " + exchange.getIn().getBody().toString());
	}
}
