package com.hsahu.camel.jms.activemq;

import org.apache.camel.CamelContext;
import org.apache.camel.spring.SpringCamelContext;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Main class to run Camel application.
 * 
 * See beans.xml
 * 
 * @author hsahu
 *
 */
public class CamelSpringApplication {

	public static void main(String[] args) throws Exception {

		/**
		 * application context loading from beans.xml file
		 */
		ApplicationContext springContext = new ClassPathXmlApplicationContext("ftp-activemq-camel-context.xml");

		/**
		 * loading camel context from application context
		 */
		CamelContext camelContext = new SpringCamelContext(springContext);

		/**
		 * start the camel context
		 */
		camelContext.start();

		/**
		 * stop camel context
		 */
		camelContext.stop();
	}
}
